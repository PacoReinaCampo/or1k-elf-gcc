/* Generated automatically. */
static const char configuration_arguments[] = "../gcc/configure --target=or1k-elf --prefix=/opt/toolchains/or1k-elf-multicore --enable-languages=c,c++ --disable-shared --disable-libssp --with-newlib";
static const char thread_model[] = "single";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { NULL, NULL} };
